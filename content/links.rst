.. Program Langs

.. _python: https://www.python.org
.. _micropython: https://micropython.org
.. _r: https://www.r-project.org
.. _javascript: https://www.javascript.com
.. _latex: https://www.latex-project.org
.. _restructuredtext: http://docutils.sourceforge.net/rst.html
.. _markdown: https://en.wikipedia.org/wiki/Markdown


.. Services

.. _github: https://github.com
.. _gitlab: https://gitlab.com
.. _bitbucket: https://bitbucket.org
.. _Let's Encrypt: https://letsencrypt.org

.. _heroku: https://www.heroku.com
.. _aws: https://aws.amazon.com

.. _odoo: https://www.odoo.com
.. _stripe: https://stripe.com
.. _nutcache: http://www.nutcache.com
.. _wave: https://www.waveapps.com


.. OS

.. _linux: https://www.linux.org
.. _raspbian: https://www.raspberrypi.org/documentation/raspbian
.. _mint: https://linuxmint.com
.. _openSUSE: https://www.opensuse.org
.. _ubuntu: https://www.ubuntu.com


.. Python packages

.. _django: https://www.djangoproject.com
.. _flask: http://flask.pocoo.org
.. _numpy: http://www.numpy.org
.. _requests: http://docs.python-requests.org
.. _pillow: https://pillow.readthedocs.io
.. _pelican: http://docs.getpelican.com/en/stable


.. JavaScript packages

.. _vue: https://vuejs.org/
.. _vuex-orm: https://vuex-orm.github.io/vuex-orm/
.. _moment.js : https://momentjs.com/

.. Platform

.. _shopify: https://www.shopify.com


.. Programs

.. _postgresql: https://www.postgresql.org
.. _git: https://git-scm.com
.. _mongodb: https://www.mongodb.com
.. _nginx: https://www.nginx.com


.. Our Links

.. _snitch'n: https://snitchn.com
.. _streamer lounge: https://gitlab.com/handycodejob/StreamerCoin
.. _chowbank: https://chowbank.io
.. _upcraft club: http://upcraft.club
.. _hcj github: https://github.com/handycodejob
.. _hcj gitlab: https://gitlab.com/handycodejob


.. Other
