HCJ Website History
###################

:date: 2018-03-17 15:16
:tags: static, pelican, python, hcj
:category: Blog
:slug: static-site
:authors: Sydney Henry, Mike Henry
:status: published
:summary: We now use Pelican for our company website

Background
**********

When we first started our company we used a enterprise resource planning system called `Odoo`_.
Odoo had a website module which would allow customers to contact us via a web form.
The website module also supported WYSIWYG editor for the site which was quite useful.
After a short time Odoo had an update and if we wanted to migrate our data, we would have to pay for it.
While Odoo was open source, it was the tacky kind of open source where they essentially had a 'freemium' business model and didn't really care about the community. 

We decided that it was not worth jumping through the various hoops to update Odoo (did we mention you had to pay for the database migration script?!?!) and looked for other solutions.
We started to utilize a few SaaS solutions like `Wave`_ and `Nutcache`_ [1]_.
These services replaced Odoo's time tacking and invoice creation functionality, but didn't provide a website.
During this time we didn't have a website.
We played around with a few static generators but never published a serious site. 
This year we want to focus on getting a few new clients, so now we are working more seriously on website.
The two options we explored was `django`_ and `pelican`_.

.. [1] Nutcache also offered an 'allways free' service, but that has been recently removed.

Django
******

We use django for most of our projects so it seemed like a good choice.
:abbr:`We (Sydney)` have always had this idea of a django app that we just keep adding new modules too and add functionality.
So we would have a :abbr:`CMS (Content Management System)` that would allow for a static site, and then a plugin for `Stripe`_ for payment processing.
This had the downside of spending development cycles to set up, as well as cost money to keep up 24/7 (darn you heroku! we miss your free dynos).
While we still hope to do something like this, right now, it is more pragmatic to get *some* site done first.


Pelican
*******

We both have some experience settings up something basic with pelican, so it seemed like a good choice as well.
Not much had to be done to get it working with Gitlab's pages.
The main edit was to the ``makefile``, making sure it used ``public`` as an output and then add in the ``.gitlab-ci.yml`` so it gets deployed.
We will post a tutorial later on how to set up a static site on Gitlab with pelican and utilizing `Let's Encrypt`_ to enable https.


.. include:: ../links.rst
