Projects
########

:tags: projects
:category: Pages
:slug: projects
:authors: Sydney Henry
:summary: Projects we have worked on
:order: 3



`Snitch'n`_
***********

Snitch'n lets you browse police images of wanted criminals, suspects, associates, missing persons, and more. Think you recognize someone? You can share on social media or send a tip to law enforcement.



`Streamer Lounge`_
******************

Allows content creators to generate income using their followers cpu power to mine crypocoins. We view this as an alternative to Patron, ads, or paid subscribers.



`Chowbank`_
***********

Help reduce food waste by connecting those with extra food to those that need it.



`Upcraft Club`_
***************

We built a url router to allow brick and mortar stores to sell digital patterns using referral links.
We also added functionality to the Shopify site with a Django Shopify plugin.


`HCJ Github`_
*************

We do a lot of open-source work.


.. include:: ../links.rst
