Home
####

:date: 2018-03-17 14:50
:modified: 2018-07-08 14:20
:tags: home
:category: Blog
:url:
:save_as: index.html
:authors: Sydney Henry
:summary: Home
:order: 1


We are a small company that started in Iowa.
Currently, siblings Mike and Sydney Henry are the employees.
We love to learn new technologies, and are always picking up new skills.
We are also accepting new clients.
Currently we are looking for some short term projects that can turn into long term relationships.

We have experience **creating**:

  *  Cryptocurrency

    *  Ethereum Token contracts
    *  Ethereum Alt coin creation and management
    *  Ethereum Alt coin wallets
    *  IRC interface for the Ethereum Network

  *  Mobile apps

    *  Backend and architecture
    *  White labeling

  *  Data analysis

    *  Data mining, scraping, and processing
    *  Modeling and visualization

  *  Automation

    *  Scripting to automate repetitive work
    *  Continuous Integration

  *  Server management

    *  Multiplayer Game servers (Terraria, Factorio, Minecraft, etc)
    *  Nginx and firewall management and design
    *  Configuring multiple services to be cost-effective with hardware usage

  *  Embedded Systems

    *  ESP-8285 board based HTTP server using sockets for small footprint (1M flash on chip)
    *  Integration with DS18B20 temperature probes with custom connectors for hot swapping
    *  Raspberry Pi command-and-control for data retention


Technologies that we are familiar with:

  *  `Python`_ (2.7, 3.3+), `django`_ (1.7+), `flask`_, `NumPy`_, `Requests`_, `Pillow`_, `Pelican`_
  *  `Micropython`_
  *  `JavaScript`_, `vue`_, `vuex-orm`_, `moment.js`_
  *  `R`_
  *  `Heroku`_
  *  `AWS`_, route 53, elastic beanstalk, s3, rds, ec2
  *  `Linux`_, `openSUSE`_, `mint`_, `Raspbian`_, `Ubuntu`_
  *  `Postgresql`_, `MongoDB`_
  *  `Nginx`_
  *  `Git`_, `GitHub`_, `GitLab`_, `BitBucket`_
  *  `LaTeX`_, HTML, CSS, `reStructuredText`_, `Markdown`_
  *  `Shopify`_

.. include:: ../links.rst
