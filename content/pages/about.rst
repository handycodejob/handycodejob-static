About Us
########

:date: 2018-03-14 17:00
:modified: 2018-07-08 14:20
:tags: about
:category: Pages
:slug: about
:authors: Sydney Henry
:summary: About Us
:order: 2


Handy Code Job, LLC was incorporated in Iowa on October 20, 2014.
Mike had graduated Simpson College in May 2014 and was working at a local computer service company in Indianola, Iowa.
Sydney was in her second year at Simpson, studying Computer Science and Mathematics.
Our "headquarters" is currently where Mike lives, in Boise, Idaho, while Sydney currently is located in Gilbert, Arizona.


Mike Henry
==========

.. image:: {filename}/static/images/mike_henry.jpg
    :align: left
    :alt: Picture of Mike Henry

Mike is currently a doctorate student at Boise State University studying Materials Science and Engineering.
Mike's research interests include self-assembly of semi-conducting organic molecules, coarse-grain computational models, and scientific software engineering. 

Sydney Henry
============

.. image:: {filename}/static/images/sydney_henry.jpg
    :align: left
    :alt: Picture of Sydney Henry

Sydney currently works on Handy Code Job projects as well as takes care of dogs.

In her free time, she is normally watching someone streaming on Twitch or catching up on some YouTube videos.
While she rarely watches TV, if left alone with one, it will surely be on Law & Order.
